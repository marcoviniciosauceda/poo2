/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author marco
 */
public class EmpleadoBase extends Empleado implements Impuesto{
    private float pagoDia;
    private float diasTrabajados;

    public EmpleadoBase() {
        this.pagoDia = 0.0f;
        this.diasTrabajados = 0.0f;
    }

    public EmpleadoBase(float pagoDia, float diasTrabajados, int numero, String nombre, String puesto, String departamento) {
        super(numero, nombre, puesto, departamento);
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
    }

   

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    

    @Override
    public float calcularPago() {
        float pagoTotal = 0.0f;
        pagoTotal = this.diasTrabajados * this.pagoDia;
        return pagoTotal;
    }

    @Override
    public float calcularImpuesto() {
        float impuestos = 0.0f;
        if (this.calcularPago() > 5000) {
            impuestos = this.calcularPago() * .16f;
        }
        return impuestos;
    }
    public float calcularTotalPago(){
        float totalPago = 0.0f;
        totalPago = this.calcularPago()- this.calcularImpuesto();
        return totalPago;
    }
    
    
    
}
