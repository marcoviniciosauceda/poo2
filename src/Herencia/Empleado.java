/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author marco
 */
public abstract class Empleado {
    protected int numero;
    protected String nombre;
    protected String puesto;
    protected String departamento;

    public Empleado() {
        this.numero = 0;
        this.nombre = "";
        this.puesto = "";
        this.departamento = "";
    }

    public Empleado(int numero, String nombre, String puesto, String departamento) {
        this.numero = numero;
        this.nombre = nombre;
        this.puesto = puesto;
        this.departamento = departamento;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
    public abstract float calcularPago();
    
    
    
}
