/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author marco
 */
public class Gasolina {
    private int id;
    private String marca;
    private int tipo;
    private float precio;
    
    public Gasolina(){
        this.id=0;
        this.marca= "";
        this.precio = 0.0f;
        this.tipo = 0;
    }
    public Gasolina(int id, String marca, int tipo, float precio){
        this.id = id;
        this.marca= marca;
        this.precio = precio;
        this.tipo = tipo;
    }
    public Gasolina(Gasolina gas){
        this.id = gas.id;
        this.marca= gas.marca;
        this.precio = gas.precio;
        this.tipo = gas.tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public String imprimirResultados(){
        String info = "";
        info = ("id: "+this.id+ "tipo"+this.tipo+"marca"+this.marca+"precio"+this.precio);
        return info;
    }
}