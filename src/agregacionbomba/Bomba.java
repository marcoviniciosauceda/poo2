/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author marco
 */
public class Bomba {
    private int numBomba;
    private float capacidad;
    private float contador;
    private Gasolina gasolina; 
    
    public Bomba(){
        this.numBomba=0;
        this.gasolina= new Gasolina();
        this.contador = 0.0f;
        this.capacidad = 0.0f;
    }
    public Bomba(int id, String marca, int tipo, float precio){
        this.capacidad = capacidad;
        this.contador= contador;
        this.gasolina = gasolina;
        this.numBomba = numBomba;
    }
    public Bomba(Bomba bom){
        this.capacidad = bom.capacidad;
        this.contador= bom.contador;
        this.gasolina = bom.gasolina;
        this.numBomba = bom.numBomba;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getContador() {
        return contador;
    }

    public void setContador(float contador) {
        this.contador = contador;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    public float obtenerInventario(){
        return this.capacidad-this.contador;
    }
    public boolean realizarVenta(float cantidad){
        boolean exito = false;
        if (cantidad <= this.obtenerInventario())exito=true;
        return exito;
    }
    public float calcularVenta(){
        float total=0.0f;
        total = this.contador * this.gasolina.getPrecio();
        return total;
    }
    
}

